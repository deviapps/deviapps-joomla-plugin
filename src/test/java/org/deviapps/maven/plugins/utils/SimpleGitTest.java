/**
 * @author 		Cyprian Śniegota
 * @copyright 	Copyright (C) 2012- HMail.pl. All rights reserved.
 * @license 	GNU/GPL
 */
package org.deviapps.maven.plugins.utils;

import java.io.IOException;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.junit.Test;

/**
 * @author Cyprian Śniegota
 * 
 */
public class SimpleGitTest {

    /**
     * @throws GitAPIException
     * @throws TransportException
     * @throws InvalidRemoteException
     * @throws IOException
     * 
     */
    @Test
    public void gitCloneTest() throws InvalidRemoteException, TransportException, GitAPIException, IOException {
        // skip this test
        // SimpleGit sg = new SimpleGit();
        // sg.cloneRepo("https://bitbucket.org/deviapps/deviapps-joomla-skel.git");
    }
}
