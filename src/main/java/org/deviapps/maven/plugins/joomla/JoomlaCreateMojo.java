/**
 * @author 		Cyprian Śniegota
 * @copyright 	Copyright (C) 2012- HMail.pl. All rights reserved.
 * @license 	GNU/GPL
 */
package org.deviapps.maven.plugins.joomla;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.deviapps.maven.plugins.utils.SimpleGit;
import org.deviapps.maven.plugins.utils.SimpleReplace;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;

/**
 * Mojo (goal) to create empty extension from SKEL. It will clone git repository
 * with latest extensions SKEL version
 * 
 * @author Cyprian Śniegota
 * 
 */
@Mojo(name = "create", threadSafe = true, defaultPhase = LifecyclePhase.NONE, requiresDependencyResolution = ResolutionScope.NONE, requiresProject = false)
public class JoomlaCreateMojo extends AbstractMojo {

    private static final String _REPO_TMP_DIR           = "_deviapps_joomla_plugin_tmp";

    private static final String _EXTENSION_SKEL_GIT_URL = "https://bitbucket.org/deviapps/deviapps-joomla-skel.git";

    /**
     * Extension type
     * 
     * @see ExtensionTypeEnum
     */
    @Parameter(defaultValue = "${extensionType}", required = false)
    private String              extensionType;

    /**
     * Skel template. There are only "basic" templates.
     */
    @Parameter(defaultValue = "basic", required = true)
    private String              extensionTemplate;

    /**
     * Required Extension Name (without extension type prefix). For "com_test"
     * type only "test"
     */
    @Parameter(defaultValue = "${extensionName}", required = true)
    private String              extensionName;

    /**
     * If true - mojo will refresh cloned version Extensions Skel set.
     */
    @Parameter(defaultValue = "${doClone}", required = false)
    private Boolean             doClone;

    public void execute() throws MojoExecutionException, MojoFailureException {
        @SuppressWarnings("rawtypes")
        Map context = this.getPluginContext();
        PluginDescriptor pd = (PluginDescriptor) context.get("pluginDescriptor");
        try {
            if (this.extensionType == null) {
                this.extensionType = "component";
            }
            ExtensionTypeEnum etm = ExtensionTypeEnum.valueOf(this.extensionType.toUpperCase());
            if (etm == null) {
                throw new MojoExecutionException("Bad extension type: " + this.extensionType);
            }
            File tmpDirectory = new File(_REPO_TMP_DIR);
            File extensionDirectory = new File(etm.getPrefix() + this.extensionName);
            if (extensionDirectory.exists()) {
                throw new MojoExecutionException("Specified project already exists");
            }
            // clone skel repository if not already there
            if (this.doClone == null) {
                this.doClone = false;
            }
            if (this.doClone || !tmpDirectory.exists()) {
                if (this.doClone && tmpDirectory.exists()) {
                    FileUtils.deleteDirectory(tmpDirectory);
                }
                this.getLog().info("Cloning from repository");
                SimpleGit sg = new SimpleGit();
                sg.cloneRepo(_EXTENSION_SKEL_GIT_URL, _REPO_TMP_DIR);
            }
            File workDir = new File(tmpDirectory.getPath() + org.codehaus.plexus.util.FileUtils.FS + this.extensionType + "-" + this.extensionTemplate);
            FileUtils.copyDirectory(workDir, extensionDirectory);
            SimpleReplace ss = new SimpleReplace();

            // do replace in files, change every "skel" string into new extension name (case variations)
            this.getLog().info("Replacing files " + extensionDirectory.getPath());
            ss.replaceAll(extensionDirectory, "skel", this.extensionName, "UTF-8");
            ss.replaceAll(extensionDirectory, "SKEL", this.extensionName.toUpperCase(), "UTF-8");
            ss.replaceAll(extensionDirectory, "Skel", this.extensionName.substring(0, 1).toUpperCase().concat(this.extensionName.substring(1)), "UTF-8");
            ss.replaceAll(extensionDirectory, "<joomla-plugin.version>.+<\\/joomla-plugin.version>", "<joomla-plugin.version>" + pd.getVersion()
                    + "</joomla-plugin.version>", "UTF-8");
            ss.replaceFilenames(extensionDirectory, "skel", this.extensionName);
        } catch (InvalidRemoteException e) {
            throw new MojoExecutionException("Error remote ", e);
        } catch (TransportException e) {
            throw new MojoExecutionException("Error transport ", e);
        } catch (GitAPIException e) {
            throw new MojoExecutionException("Error git ", e);
        } catch (IOException e) {
            throw new MojoExecutionException("Error io ", e);
        }

    }
}
