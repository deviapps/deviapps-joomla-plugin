/**
 * @author      Cyprian Śniegota
 * @copyright   Copyright (C) 2012- HMail.pl. All rights reserved.
 * @license     GNU/GPL
 */
package org.deviapps.maven.plugins.joomla;

/**
 * Enumerate extension types. Extra CORE type for core file replacement.
 * 
 * @author Cyprian Śniegota
 * 
 */
public enum ExtensionTypeEnum {
    /**
     * Component
     */
    COMPONENT("component", "com_"),
    /**
     * Module
     */
    MODULE("module", "mod_"),
    /**
     * Plugin
     */
    PLUGIN("plugin", "plg_"),
    /**
     * Template
     */
    TEMPLATE("template", "tpl_"),
    /**
     * Library
     */
    LIBRARY("library", ""),
    /**
     * Core
     */
    CORE("core", ""),
    /**
     * Custom
     */
    CUSTOM("custom", "");
    ;

    private String name;
    private String prefix;

    private ExtensionTypeEnum(String type, String prefix) {
        this.name = type;
        this.prefix = prefix;
    }

    public String getName() {
        return this.name;
    }

    public String getPrefix() {
        return this.prefix;
    }
}
