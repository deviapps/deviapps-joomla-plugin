/*
 * Copyright 2014 HMail.pl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.deviapps.maven.plugins.joomla;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.plexus.util.FileUtils;

/**
 * Main plugin goal - auto deploy (copy) to local Joomla! installation dir.
 * 
 * @author Cyprian Śniegota
 */
@Mojo(name = "deploy", threadSafe = true, defaultPhase = LifecyclePhase.PACKAGE, requiresDependencyResolution = ResolutionScope.TEST)
public class JoomlaDeployMojo extends AbstractMojo {

    private static final String BASE_DESTINATION_DIR_PLUGIN = "plugins/";
    private static final String BASE_DESTINATION_DIR_LIBRARY = "libraries/";
    private static final String BASE_DESTINATION_DIR_COMPONENT_ADMIN = "administrator/components/";
    private static final String BASE_DESTINATION_DIR_COMPONENT_SITE = "components/";
    private static final String BASE_DESTINATION_DIR_MODULE = "modules/";
    private static final String BASE_DESTINATION_DIR_TEMPLATE = "templates/";
    /**
     * Extension type
     * 
     * @see ExtensionTypeEnum
     */
    @Parameter(defaultValue = "component", required = true)
    private String extensionType;

    /**
     * Source directory - advanced setting
     */
    @Parameter(defaultValue = "${project.build.directory}/classes", required = true)
    private File   sourceDirectory;

    /**
     * Destination dir - advanced setting
     */
    @Parameter(defaultValue = "", required = false)
    private String customDestinationDirectory;

    /**
     * Component source dir - advanced setting
     */
    @Parameter(defaultValue = "${project.build.directory}/classes/admin", required = true)
    private File   adminComponentSourceDirectory;

    /**
     * Component source dir - advanced setting
     */
    @Parameter(defaultValue = "${project.build.directory}/classes/site", required = true)
    private File   siteComponentSourceDirectory;

    /**
     * Project name and extension destination directory name
     */
    @Parameter(defaultValue = "${project.name}", required = true)
    private String projectName;

    /**
     * Project name and extension destination directory name
     */
    @Parameter(defaultValue = "", required = false)
    private String pluginGroup;

    /**
     * Main setting - Joomla running site local directory path
     */
    @Deprecated
    @Parameter(defaultValue = "${project.build.directory}/joomlahome", required = true)
    private File   testDestinationDir;

    /**
     * Main setting - Joomla Home
     */
    @Parameter(defaultValue = "${project.build.directory}/joomlahome", required = true)
    private File joomlaHome;

    private final Map<ExtensionTypeEnum, String> destinationPaths = new HashMap<>();

    public void execute() throws MojoExecutionException {
        //setup dest dir map
        destinationPaths.put(ExtensionTypeEnum.CUSTOM, this.customDestinationDirectory);
        destinationPaths.put(ExtensionTypeEnum.MODULE, BASE_DESTINATION_DIR_MODULE + projectName);
        String pluginDir = this.pluginGroup+"/"+projectName.replaceAll("plg_","");
        destinationPaths.put(ExtensionTypeEnum.PLUGIN, BASE_DESTINATION_DIR_PLUGIN + pluginDir);
        destinationPaths.put(ExtensionTypeEnum.TEMPLATE, BASE_DESTINATION_DIR_TEMPLATE + projectName);
        destinationPaths.put(ExtensionTypeEnum.LIBRARY, BASE_DESTINATION_DIR_LIBRARY + projectName);


        // extensionType can't be null; there is some default value in
        // definition.
        ExtensionTypeEnum etm = ExtensionTypeEnum.valueOf(this.extensionType.toUpperCase());
        if (etm == null) {
            throw new MojoExecutionException("Error parsing extension type: " + this.extensionType);
        }
        getLog().info("Running type - " + etm.getName());
        switch (etm) {
            case COMPONENT:
                deployComponent();
                break;
            case MODULE:
            case PLUGIN:
            case TEMPLATE:
            case CUSTOM:
            case LIBRARY:
                deploy(this.destinationPaths.get(etm));
                break;
            case CORE:
            default:
                throw new MojoExecutionException("[" + etm.getName() + "] Not implemented yet! Contact developer team.");
        }
        getLog().info("--- Joomla deploy - all done.");
    }

    private void deployComponent() throws MojoExecutionException {
        // check conditions
        if (!this.adminComponentSourceDirectory.exists()) {
            throw new MojoExecutionException("No admin component folder found: " + this.adminComponentSourceDirectory.getAbsolutePath());
        }
        if (!this.siteComponentSourceDirectory.exists()) {
            throw new MojoExecutionException("No site component folder found: " + this.adminComponentSourceDirectory.getAbsolutePath());
        }
        File adminDest = obtainDestinationDirectory(BASE_DESTINATION_DIR_COMPONENT_ADMIN + projectName);
        File siteDest = obtainDestinationDirectory(BASE_DESTINATION_DIR_COMPONENT_SITE + projectName);
        // do job: copy modified files
        copyToDestination(adminComponentSourceDirectory, adminDest);
        copyToDestination(siteComponentSourceDirectory, siteDest);
    }

    private void deploy(String destinationDirectory) throws MojoExecutionException {
        // check conditions
        File destCustom = obtainDestinationDirectory(destinationDirectory);
        // do job: copy modified files
        copyToDestination(destCustom);
    }

    private void copyToDestination(File destination) throws MojoExecutionException {
        this.copyToDestination(this.sourceDirectory, destination);
    }

    private void copyToDestination(File source, File destination) throws MojoExecutionException {
        try {
            getLog().info("Copying: " + source.getPath() + " to: " + destination.getPath());
            FileUtils.copyDirectoryStructureIfModified(source, destination);
        } catch (IOException e) {
            throw new MojoExecutionException("Error copying files ", e);
        }
    }

    private File obtainDestinationDirectory(String destinationDirectory) throws MojoExecutionException {
        File destCustom = new File(this.joomlaHome.getPath() + FileUtils.FS + destinationDirectory);
        if (!this.sourceDirectory.exists()) {
            throw new MojoExecutionException("No source folder found: " + this.sourceDirectory.getAbsolutePath());
        }
        if (!destCustom.exists()) {
            getLog().info("Creating destination dir: "+destCustom.getPath());
            if (!destCustom.mkdirs()) {
                getLog().error("Error creating destination dir ["+destCustom.getPath()+"]");
            }
        }
        return destCustom;
    }
}
