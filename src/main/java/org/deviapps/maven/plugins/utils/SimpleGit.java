/**
 * @author 		Cyprian Śniegota
 * @copyright 	Copyright (C) 2012- HMail.pl. All rights reserved.
 * @license 	GNU/GPL
 */
package org.deviapps.maven.plugins.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;

/**
 * Git repo clone helper
 *
 * @author Cyprian Śniegota
 * 
 */
public class SimpleGit {
    /**
     * clone and checkout HEAD
     */
    public void cloneRepo(String repoUrl, String folderName) throws InvalidRemoteException, TransportException, GitAPIException, IOException {
        File tmpDest = new File(folderName);
        FileUtils.deleteDirectory(tmpDest);
        CloneCommand cloneCommand = new CloneCommand().setURI(repoUrl).setNoCheckout(false).setBare(false).setDirectory(tmpDest);
        Git git = cloneCommand.call();
        git.checkout();
    }
}
