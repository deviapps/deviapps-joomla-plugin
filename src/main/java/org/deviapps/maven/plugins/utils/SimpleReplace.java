/**
 * Based on Lenya SED.java,v 1.3
 * @author 		Cyprian Śniegota
 * @copyright 	Copyright (C) 2012- HMail.pl. All rights reserved.
 * @license 	GNU/GPL
 */
package org.deviapps.maven.plugins.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

/**
 * 
 * @author Cyprian Śniegota
 * 
 */
public class SimpleReplace {

    /**
     * Work only with those file extensions
     */
	private static final String[] EXTENSIONS = { "xml", "php", "ini" };

    /**
     * Replace pattern in all files from selected directory root (recursive in subdirectories)
     *
     * @param root
     * @param findPattern
     * @param replacement
     * @param encoding
     * @throws IOException
     */
	public void replaceAll(File root, String findPattern, String replacement,
			String encoding) throws IOException {
		String[] extensions = EXTENSIONS;
		boolean recursive = true;
		@SuppressWarnings("unchecked")
		Collection<File> files = FileUtils.listFiles(root, extensions,
				recursive);
		for (File file : files) {
			this.replaceInFile(file, findPattern, replacement, encoding);
		}
	}

    /**
     * Rename files in selected directory,
     * change findPattern to replacement in filenames
     *
     * @param root
     * @param findPattern
     * @param replacement
     * @throws IOException
     */
	public void replaceFilenames(File root, String findPattern,
			String replacement) throws IOException {
		String[] extensions = EXTENSIONS;
		boolean recursive = true;
		Pattern pattern = Pattern.compile(findPattern);
		@SuppressWarnings("unchecked")
		Collection<File> files = FileUtils.listFiles(root, extensions,
				recursive);
		for (File file : files) {
			String path = file.getPath();
			Matcher matcher = pattern.matcher(path);
			String newFileName = matcher.replaceAll(replacement);
			if (!path.equals(newFileName)) {
				File newFile = new File(newFileName);
				FileUtils.copyFile(file, newFile);
				FileUtils.forceDelete(file);
			}
		}
	}

    /**
     * Replace pattern in selected file body into replacement
     *
     * @param file
     * @param findPattern
     * @param replacement
     * @param encoding
     * @throws IOException
     */
	public void replaceInFile(File file, String findPattern,
			String replacement, String encoding) throws IOException {
		Pattern pattern = Pattern.compile(findPattern);

		/* */
		StringBuffer fileContent = new StringBuffer();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				new FileInputStream(file), encoding));
		String str;
		while ((str = in.readLine()) != null) {
			fileContent.append(str).append('\n');
		}
		in.close();
		Matcher matcher = pattern.matcher(fileContent);
		/*
		 * FileInputStream fis = new FileInputStream(file); FileChannel fc =
		 * fis.getChannel(); int sz = (int) fc.size(); MappedByteBuffer bb =
		 * fc.map(FileChannel.MapMode.READ_ONLY, 0, sz); fis.close(); Charset
		 * charset = Charset.forName(encoding); // UTF-8 CharsetDecoder decoder
		 * = charset.newDecoder(); CharBuffer cb = decoder.decode(bb); Matcher
		 * matcher = pattern.matcher(cb);
		 */
		String outString = matcher.replaceAll(replacement);
		FileOutputStream fos = new FileOutputStream(file.getAbsolutePath());
		PrintStream ps = new PrintStream(fos);
		ps.print(outString);
		ps.close();
		fos.close();
	}
}
